#ifndef __std_ints_hpp
#define __std_ints_hpp

#include <ostream>
#include <istream>

namespace std {

#include <stdint.h>

inline ostream& operator <<(ostream& stream, const uint8_t& small)
{
  return stream << (unsigned int) small;
}

inline ostream& operator <<(ostream& stream, const int8_t& small)
{
  return stream << (int) small;
}

inline istream& operator >>(istream& stream, uint8_t& small)
{
  int n;
  stream >> n;
  small = n;
  return stream;
}

inline istream& operator >>(istream& stream, int8_t& small)
{
  int n;
  stream >> n;
  small = n;
  return stream;
}

}

#endif
